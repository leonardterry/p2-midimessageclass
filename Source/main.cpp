//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>

class MidiMessage{
public:
    MidiMessage() //Constructor
    {
        std::cout << "Constructor\n";
        number = 60;
        velocity = 0;
        channel = 1;
    }
    ~MidiMessage() //Destructor
    {
        std::cout << "Destructor\n";
    }
    void setNoteNumber(int value) //Mutator
    {
        if (value >= 127)
            number = 127;
        else if (value <= 0)
            number = 0;
        else
            number = value;
    }
    void setVelocity(int value) //Mutator
    {
        if (value >= 127)
            velocity = 127;
        else if (value <= 0)
            velocity = 0;
        else
            velocity = value;
    }
    void setChannel(int value) //Mutator
    {
        if (value >= 127)
            channel = 127;
        else if (value <= 0)
            channel = 0;
        else
            channel = value;
    }
    int getNoteNumber() const //Accessor
    {
        return number;
    }
    int getChannel() const //Accessor
    {
        return channel;
    }
    int getVelocity() const //Accessor
    {
        return velocity;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    float getFloatVelocity() const //Accessor
    {
        return velocity/127;
    }
private:  //variables must be private
    int number;
    int velocity;
    int channel;
};



int main (int argc, const char* argv[])
{
    MidiMessage note;
    
    int newNote;
    int newChan;
    float newVel;
    
    //constructor values
    std::cout << "note: " << note.getNoteNumber() << std::endl;
    std::cout << "velocity: " << note.getVelocity() << std::endl;
    std::cout << "channel: " << note.getChannel() << std::endl;
    
    //user entry
    std::cout << "\nenter note: ";
    std::cin >> newNote;
    std::cout << "enter amplitude: ";
    std::cin >>  newVel;
    std::cout << "enter channel: ";
    std::cin >> newChan;
    
    //process functions
    note.setNoteNumber(newNote);
    note.setChannel(newChan);
    note.setVelocity(newVel);
    
    //new values in console
    std::cout << "\nnew note: " << note.getNoteNumber() << std::endl;
    std::cout << "new velocity: " << note.getVelocity() << std::endl;
    std::cout << "new channel: " << note.getChannel() << std::endl;
    
    return 0;
}

